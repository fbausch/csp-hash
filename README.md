# CSP Hash

Creates hashes of inline script and inline style tags in websites and HTML files that can be used easily in CSP (Content Security Policy) headers.

## Usage

```
usage: csp-hash.py [-h] [-u URL] [-f FILE] [-s SHA]

optional arguments:
  -h, --help            show this help message and exit
  -u URL, --url URL     Specify a URL to retrieve. Can be used several times.
  -f FILE, --file FILE  Specify an HTML file to load. Can be used several times.
  -s SHA, --sha SHA     Specify the SHA algorithm (supported: 256, 384, 512).
```