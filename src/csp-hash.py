#!/usr/bin/env python3

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

#   csp-hash.py
# © Copyright 2021 Florian Bausch

from urllib import request
from lxml import html
import hashlib
import base64
import sys
import argparse


def compute_shas(htmls, shastrength, shafun, typ):
    eles = []
    # Look for the tags (script or style)
    print(f"look for {typ} tags")
    for h in htmls:
        eles += h.xpath(f"//{typ}")
    leneles = len(eles)
    print(f"compute SHA for {leneles} {typ} tags")
    # Add default values for this CSP element
    shas = [f"{typ}-src", "'self'"]
    # Check every script/style tag
    for ele in eles:
        text = ele.text
        # Check if there is actual content
        if text is None:
            continue
        # Remove all \r because they are ignored during the hash computation
        text = text.replace("\r", "")
        # Compute hash and encode it via base64
        sha = base64.b64encode(shafun(text.encode("utf-8")).digest()).decode('utf-8')
        entry = f"'sha{shastrength}-{sha}'"
        # Look for duplicates
        if entry not in shas:
            shas.append(f"'sha{shastrength}-{sha}'")
    if len(shas) > 2:
        # If there are script/style tags that we computet hashes for,
        # we append 'unsafe-inline' just to have a backwards compatibility.
        # It will be ignored by modern browsers if hashes are defined in CSP.
        shas.append("'unsafe-inline'")

    # Print the CSP element
    print("%s;" % ' '.join(shas))


if __name__ == "__main__":
    supported_shas = {"256": hashlib.sha256,
                      "384": hashlib.sha384,
                      "512": hashlib.sha512}
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--url", action='append', help="Specify a URL to retrieve. Can be used several times.")
    parser.add_argument("-f", "--file", action='append', help="Specify an HTML file to load. Can be used several times.")
    parser.add_argument("-s", "--sha", help="Specify the SHA algorithm (supported: %s)." % ", ".join(supported_shas.keys()))
    args = parser.parse_args()

    if args.sha not in supported_shas.keys():
        print("ERROR. Use a supported SHA algorithm.")
        sys.exit(1)
    if args.url is None and args.file is None:
        print("ERROR. Give at least one URL or HTML file.")
        sys.exit(1)

    htmls = []
    # If we got any URLs, retrieve data from there and parse the HTML.
    if args.url is not None:
        for url in args.url:
            print(f"retrieve data from {url}")
            htmls.append(html.fromstring(request.urlopen(url).read()))
    # If we got any files, retrieve data from there and parse the HTML.
    if args.file is not None:
        for file in args.file:
            print(f"read data from {file}")
            with open(file, 'r') as f:
                htmls.append(html.fromstring(f.read()))

    # Look for script tags and compute hashes
    compute_shas(htmls, args.sha, supported_shas[args.sha], "script")
    print("\n\n --- \n\n")
    # Look for style tags and compute hashes
    compute_shas(htmls, args.sha, supported_shas[args.sha], "style")
